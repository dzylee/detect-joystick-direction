/*
The purpose of this program is to provide visual indications to the user of
when the joystick on the Zen cape is being pushed to the "up" position and when
it is not. To determine whether the joystick is in the "up" position or not,
the program repeatedly polls (reads) the value that is currently on the GPIO
pin that corresponds to the joystick's "up" position. The pin's value will
either be a 1 (meaning the joystick is in the "down" position) or a 0
(meaning the joystick is not in the "up" position).

Whenever the program detects that the joystick is in the "up" position, it will
flash the LED0 light on the BeagleBone Green board three times in a row, in
quick succession. When the joystick is in any other position, the program will
only flash the light once. In both cases, after the program has performed the
flashes, it will pause briefly before polling the state of the joystick again.
The program repeats this cycle indefinitely until it determines that the
joystick has been in the "up" position for 10 consecutive cycles, and at which
point, the program will terminate.

In addition to flashing the LED0 light, the program will also print a line of
information to the terminal after each polling of the GPIO pin. In the case
when the joystick is in the "up" position, the line of information will state
three pieces of information: the number of flashes made (3 in this case), a
value corresponding to the joystick's "up" position (1 in this case), and the
number of consecutive cycles that the joystick has been detected in the "up"
position for. In the case when the joystick is any other position, the values
for these three pieces of information will be 0, 0, and 0.
*/


#include <stdio.h>
#include <stdbool.h>
#include <time.h>


#define GPIO_EXPORT_FILE_PATHNAME "/sys/class/gpio/export"
#define GPIO_PIN26_DIRECTION_FILE_PATHNAME "/sys/class/gpio/gpio26/direction"
#define GPIO_PIN26_VALUE_FILE_PATHNAME "/sys/class/gpio/gpio26/value"
#define LED0_BRIGHTNESS_FILE_PATHNAME "/sys/class/leds/beaglebone:green:usr0/brightness"
#define LED0_TRIGGER_FILE_PATHNAME "/sys/class/leds/beaglebone:green:usr0/trigger"
#define LED0_DELAYON_FILE_PATHNAME "/sys/class/leds/beaglebone:green:usr0/delay_on"
#define LED0_DELAYOFF_FILE_PATHNAME "/sys/class/leds/beaglebone:green:usr0/delay_off"


/*
Determines whether the joystick is currently in the "up" position. It does this
by reading the value on GPIO pin 26. If the joystick is in the "up" position,
the function returns true, and false otherwise.
*/
_Bool joystickPushedUp()
{   FILE* ptr_GPIO26_value_file = fopen(GPIO_PIN26_VALUE_FILE_PATHNAME, "r");
    if( ptr_GPIO26_value_file == NULL )
    {   printf("Failed to open the file %s\n", GPIO_PIN26_VALUE_FILE_PATHNAME);
        return 1;
    }

    const int MAX_LINE_LENGTH = 1024;
    char lineBuffer[MAX_LINE_LENGTH];
    fgets(lineBuffer, MAX_LINE_LENGTH, ptr_GPIO26_value_file);

    fclose(ptr_GPIO26_value_file);

    if( lineBuffer[0] == '0' )
    {   return 1;
    }
    else // lineBuffer[0] == '1'
    {   return 0;
    }
}

/*
Flashes the LED0 light on the BeagleBone Green board once. This function is
used for the case when the program detects that the joystick is not in the "up"
position.
*/
int singleFlash()
{   int fileWriteStatus = 0;
    const int ON_AND_OFF_LENGTH_MILLISECS = 100;

    FILE* ptr_LED0_trigger_file = fopen(LED0_TRIGGER_FILE_PATHNAME, "r+");
    if( ptr_LED0_trigger_file == NULL )
    {   printf("Failed to open the file %s\n", LED0_TRIGGER_FILE_PATHNAME);
        return 1;
    }

    fileWriteStatus = fprintf(ptr_LED0_trigger_file, "timer"); //The "timer" setting causes LED0 to repeatedly alternate between turning the light on and off.
    if( fileWriteStatus <= 0 )
    {   printf("Failed to write to the file %s\n", LED0_TRIGGER_FILE_PATHNAME);
        fclose(ptr_LED0_trigger_file);
        return 1;
    }

    fclose(ptr_LED0_trigger_file);


    FILE* ptr_LED0_delayon_file = fopen(LED0_DELAYON_FILE_PATHNAME, "r+");
    if( ptr_LED0_delayon_file == NULL )
    {   printf("Failed to open the file %s\n", LED0_DELAYON_FILE_PATHNAME);
        return 1;
    }

    fileWriteStatus = fprintf(ptr_LED0_delayon_file, "%d", ON_AND_OFF_LENGTH_MILLISECS); //Set length of on time.
    if( fileWriteStatus <= 0 )
    {   printf("Failed to write to the file %s\n", LED0_DELAYON_FILE_PATHNAME);
        fclose(ptr_LED0_delayon_file);
        return 1;
    }

    fclose(ptr_LED0_delayon_file);


    FILE* ptr_LED0_delayoff_file = fopen(LED0_DELAYOFF_FILE_PATHNAME, "r+");
    if( ptr_LED0_delayoff_file == NULL )
    {   printf("Failed to open the file %s\n", LED0_DELAYOFF_FILE_PATHNAME);
        return 1;
    }

    fileWriteStatus = fprintf(ptr_LED0_delayoff_file, "%d", ON_AND_OFF_LENGTH_MILLISECS); //Set length of off time.
    if( fileWriteStatus <= 0 )
    {   printf("Failed to write to the file %s\n", LED0_DELAYOFF_FILE_PATHNAME);
        fclose(ptr_LED0_delayoff_file);
        return 1;
    }

    fclose(ptr_LED0_delayoff_file);

    //Let the LED0 flashes run for 100 milliseconds:
    long seconds = 0;
    long nanoseconds = 100000000;
    struct timespec reqDelay = {seconds, nanoseconds};
    nanosleep(&reqDelay, (struct timespec *) NULL);


    ptr_LED0_trigger_file = fopen(LED0_TRIGGER_FILE_PATHNAME, "r+");
    if( ptr_LED0_trigger_file == NULL )
    {   printf("Failed to open the file %s\n", LED0_TRIGGER_FILE_PATHNAME);
        return 1;
    }

    fileWriteStatus = fprintf(ptr_LED0_trigger_file, "none"); //Shut off the LED0 light.
    if( fileWriteStatus <= 0 )
    {   printf("Failed to write to the file %s\n", LED0_TRIGGER_FILE_PATHNAME);
        fclose(ptr_LED0_trigger_file);
        return 1;
    }

    fclose(ptr_LED0_trigger_file);


    //Wait for 700 milliseconds before letting the program attempt to poll
    //the state of the joystick again.
    long nanoseconds2 = 700000000;
    struct timespec reqDelay2 = {seconds, nanoseconds2};
    nanosleep(&reqDelay2, (struct timespec *) NULL);

    return 0;
}


/*
Flashes the LED0 light on the BeagleBone Green board three times in quick
succession. This function is used for the case when the program detects that
the joystick is in the "up" position.
*/
int tripleFlash()
{   int fileWriteStatus = 0;
    const int ON_AND_OFF_LENGTH_MILLISECS = 100;

    FILE* ptr_LED0_trigger_file = fopen(LED0_TRIGGER_FILE_PATHNAME, "r+");
    if( ptr_LED0_trigger_file == NULL )
    {   printf("Failed to open the file %s\n", LED0_TRIGGER_FILE_PATHNAME);
        return 1;
    }

    fileWriteStatus = fprintf(ptr_LED0_trigger_file, "timer"); //The "timer" setting causes LED0 to repeatedly alternate between turning the light on and off.
    if( fileWriteStatus <= 0 )
    {   printf("Failed to write to the file %s\n", LED0_TRIGGER_FILE_PATHNAME);
        fclose(ptr_LED0_trigger_file);
        return 1;
    }

    fclose(ptr_LED0_trigger_file);


    FILE* ptr_LED0_delayon_file = fopen(LED0_DELAYON_FILE_PATHNAME, "r+");
    if( ptr_LED0_delayon_file == NULL )
    {   printf("Failed to open the file %s\n", LED0_DELAYON_FILE_PATHNAME);
        return 1;
    }

    fileWriteStatus = fprintf(ptr_LED0_delayon_file, "%d", ON_AND_OFF_LENGTH_MILLISECS); //Set length of on time.
    if( fileWriteStatus <= 0 )
    {   printf("Failed to write to the file %s\n", LED0_DELAYON_FILE_PATHNAME);
        fclose(ptr_LED0_delayon_file);
        return 1;
    }

    fclose(ptr_LED0_delayon_file);


    FILE* ptr_LED0_delayoff_file = fopen(LED0_DELAYOFF_FILE_PATHNAME, "r+");
    if( ptr_LED0_delayoff_file == NULL )
    {   printf("Failed to open the file %s\n", LED0_DELAYOFF_FILE_PATHNAME);
        return 1;
    }

    fileWriteStatus = fprintf(ptr_LED0_delayoff_file, "%d", ON_AND_OFF_LENGTH_MILLISECS); //Set length of off time.
    if( fileWriteStatus <= 0 )
    {   printf("Failed to write to the file %s\n", LED0_DELAYOFF_FILE_PATHNAME);
        fclose(ptr_LED0_delayoff_file);
        return 1;
    }

    fclose(ptr_LED0_delayoff_file);


    //Let the LED0 flashes run for 500 milliseconds:
    long seconds = 0;
    long nanoseconds = 500000000;
    struct timespec reqDelay = {seconds, nanoseconds};
    nanosleep(&reqDelay, (struct timespec *) NULL);


    ptr_LED0_trigger_file = fopen(LED0_TRIGGER_FILE_PATHNAME, "r+");
    if( ptr_LED0_trigger_file == NULL )
    {   printf("Failed to open the file %s\n", LED0_TRIGGER_FILE_PATHNAME);
        return 1;
    }

    fileWriteStatus = fprintf(ptr_LED0_trigger_file, "none");
    if( fileWriteStatus <= 0 )
    {   printf("Failed to write to the file %s\n", LED0_TRIGGER_FILE_PATHNAME); //Shut off the LED0 light.
        fclose(ptr_LED0_trigger_file);
        return 1;
    }

    fclose(ptr_LED0_trigger_file);


    //Wait for 700 milliseconds before letting the program attempt to poll
    //the state of the joystick again.
    long nanoseconds2 = 700000000;
    struct timespec reqDelay2 = {seconds, nanoseconds2};
    nanosleep(&reqDelay2, (struct timespec *) NULL);

    return 0;
}


/*
This where the program begins. It starts by greeting the world, and then
immediately proceeds to prepare LED0 light and GPIO pin 24 for use. Once that
is done, it begins monitoring the status of the joystick, and flashes LED0 as
needed and prints lines of information to the terminal accordingly.
*/
int main(int argc, char* args[])
{   printf("Hello, embedded world, from Zeng-Yuan (David) Lee!\n");

    int fileWriteStatus = 0; //To store the return value from the fprintf() function.

    //Set up pin 14 on the P8 expansion header (corresponding to Up push on joystick) for use as GPIO:
    FILE* ptr_GPIO_export_file = fopen(GPIO_EXPORT_FILE_PATHNAME, "w");
    if( ptr_GPIO_export_file == NULL )
    {   printf("Failed to open the file %s\n", GPIO_EXPORT_FILE_PATHNAME);
        return 1;
    }

    const int LINUX_GPIO_NUMBER_FOR_JOYSTICK_UP = 26; //26 is the Linux GPIO number corresponding to pin 14 on the P8 expansion header.

    fileWriteStatus = fprintf(ptr_GPIO_export_file, "%d", LINUX_GPIO_NUMBER_FOR_JOYSTICK_UP);
    if( fileWriteStatus <= 0 )
    {   printf("Failed to write to the file %s\n", GPIO_EXPORT_FILE_PATHNAME);
        fclose(ptr_GPIO_export_file);
        return 1;
    }

    fclose(ptr_GPIO_export_file);


    FILE* ptr_LED0_brightness_file = fopen(LED0_BRIGHTNESS_FILE_PATHNAME, "r+");
    if( ptr_LED0_brightness_file == NULL )
    {   printf("Failed to open the file %s\n", LED0_BRIGHTNESS_FILE_PATHNAME);
        return 1;
    }

    fileWriteStatus = fprintf(ptr_LED0_brightness_file, "0"); //Shut off LED0.
    if( fileWriteStatus <= 0 )
    {   printf("Failed to write to the file %s\n", LED0_BRIGHTNESS_FILE_PATHNAME);
        fclose(ptr_LED0_brightness_file);
        return 1;
    }

    fclose(ptr_LED0_brightness_file);


    FILE* ptr_GPIO26_direction_file = fopen(GPIO_PIN26_DIRECTION_FILE_PATHNAME, "r+");
    if( ptr_GPIO26_direction_file == NULL )
    {   printf("Failed to open the file %s\n", GPIO_PIN26_DIRECTION_FILE_PATHNAME);
        return 1;
    }

    fileWriteStatus = fprintf(ptr_GPIO26_direction_file, "in"); //Ready GPIO pin 26 for reading from.
    if( fileWriteStatus <= 0 )
    {   printf("Failed to write to the file %s\n", GPIO_PIN26_DIRECTION_FILE_PATHNAME);
        fclose(ptr_GPIO26_direction_file);
        return 1;
    }

    fclose(ptr_GPIO26_direction_file);



    //The core part of the program where the status of the joystick is repeatedly polled:
    int numConsecutiveIterations = 0; //To store the longest number of consecutive iterations that contain an Up push of the joystick.
    _Bool joystickUpStatus = 0;
    while(1)
    {   if( (joystickUpStatus = joystickPushedUp()) )
        {   numConsecutiveIterations++;

            printf("Flashing 3 times:  Joystick = %d & consecutive iterations = %d\n", joystickUpStatus, numConsecutiveIterations);
            tripleFlash();
            if( numConsecutiveIterations == 10 )
            {   printf("The joystick has been pushed up for 10 consecutive iterations of the loop. Exiting program.\n");
                break;
            }
        }
        else
        {   numConsecutiveIterations = 0;
            printf("Flashing 1 time:   Joystick = %d & consecutive iterations = %d\n", joystickUpStatus, numConsecutiveIterations);
            singleFlash();
        }
    }

    return 0;
}