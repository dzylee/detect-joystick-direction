#The C cross-compiler being used:
C_XCOMPILER=arm-linux-gnueabihf-gcc

#Compiler flags to use:
COMP_FLAGS=-Wall -g -std=c99 -D _POSIX_C_SOURCE=200809L -Werror

#Default target:
awesomeness:
	$(C_XCOMPILER) $(COMP_FLAGS) helloworld.c -o helloworld_target
